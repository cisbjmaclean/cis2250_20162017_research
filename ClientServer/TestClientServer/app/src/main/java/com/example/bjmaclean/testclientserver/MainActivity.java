package com.example.bjmaclean.testclientserver;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.bjmaclean.testclientserver.com.example.bjmaclean.testclientserver.util.ConnectionUtil;
import com.example.bjmaclean.testclientserver.com.example.bjmaclean.testclientserver.util.Tournament;
import com.example.bjmaclean.testclientserver.com.example.bjmaclean.testclientserver.util.TournamentAdapter;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Button button1 = null;
    private List<Tournament> tournamentList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TournamentAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //**************************************************************
        //from: http://www.androidhive.info/2016/01/android-working-with-recycler-view/
        //**************************************************************

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mAdapter = new TournamentAdapter(tournamentList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        String tournamentJson = ConnectionUtil.getTournaments(getApplicationContext());
        Toast.makeText(getApplicationContext(), tournamentJson, Toast.LENGTH_LONG).show();

        //*************************************************
        //Next want to take the json and convert it to some Tournament objects.
        //*************************************************

        Gson gson = new Gson();
        Tournament[] tournamentArray = gson.fromJson(tournamentJson, Tournament[].class);
        for(Tournament tournament: tournamentArray){
            Log.d("Added tournament",""+tournament);
            tournamentList.add(tournament);
        }
        tournamentList.add(new Tournament(3,"inserted","add","start","end"));

        mAdapter.notifyDataSetChanged();






        button1 = (Button) findViewById((R.id.button));

        button1.setOnClickListener(new View.OnClickListener() {
                                       public void onClick(View view) {
                                           Log.d("BJTEST", "The button was clicked");
                                           boolean isConnectedResult = ConnectionUtil.isConnected(view.getContext());
                                           Log.d("inConnectedResult=", "" + isConnectedResult);

                                           String tournamentJson = ConnectionUtil.getTournaments(view.getContext());
                                           Toast.makeText(view.getContext(), tournamentJson, Toast.LENGTH_LONG).show();

                                           //*************************************************
                                           //Next want to take the json and convert it to some Tournament objects.
                                           //*************************************************

                                           Gson gson = new Gson();
                                           Tournament[] tournamentArray = gson.fromJson(tournamentJson, Tournament[].class);
                                           Log.d("back from gson", "number of tournaments=" + tournamentArray.length);
                                       }
                                   }
        );
    }


}
