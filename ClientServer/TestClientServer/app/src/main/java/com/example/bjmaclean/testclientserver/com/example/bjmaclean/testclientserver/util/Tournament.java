/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.bjmaclean.testclientserver.com.example.bjmaclean.testclientserver.util;

import java.io.Serializable;

/**
 *
 * @author sarsenault112452
 */
public class Tournament implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer tournamentId;
    private String tournamentName;
    private String tournamentAddress;
    private String tournamentStartDate;
    private String tournamentEndDate;

    public Tournament() {
    }

    public Tournament(Integer tournamentId) {
        this.tournamentId = tournamentId;
    }

    public Tournament(Integer tournamentId, String tournamentName, String tournamentAddress, String tournamentStartDate, String tournamentEndDate) {
        this.tournamentId = tournamentId;
        this.tournamentName = tournamentName;
        this.tournamentAddress = tournamentAddress;
        this.tournamentStartDate = tournamentStartDate;
        this.tournamentEndDate = tournamentEndDate;
    }

    public Integer getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(Integer tournamentId) {
        this.tournamentId = tournamentId;
    }

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    public String getTournamentAddress() {
        return tournamentAddress;
    }

    public void setTournamentAddress(String tournamentAddress) {
        this.tournamentAddress = tournamentAddress;
    }

    public String getTournamentStartDate() {
        return tournamentStartDate;
    }

    public void setTournamentStartDate(String tournamentStartDate) {
        this.tournamentStartDate = tournamentStartDate;
    }

    public String getTournamentEndDate() {
        return tournamentEndDate;
    }

    public void setTournamentEndDate(String tournamentEndDate) {
        this.tournamentEndDate = tournamentEndDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tournamentId != null ? tournamentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tournament)) {
            return false;
        }
        Tournament other = (Tournament) object;
        if ((this.tournamentId == null && other.tournamentId != null) || (this.tournamentId != null && !this.tournamentId.equals(other.tournamentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Tournament[ tournamentId=" + tournamentId + ", tournamentName=" + tournamentName + ", tournamentAddress=" + tournamentAddress + ", tournamentStartDate=" + tournamentStartDate + ", tournamentEndDate=" + tournamentEndDate + " ]";
    }

}
