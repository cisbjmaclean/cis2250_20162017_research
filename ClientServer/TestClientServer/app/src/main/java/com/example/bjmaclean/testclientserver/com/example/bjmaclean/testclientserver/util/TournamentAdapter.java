package com.example.bjmaclean.testclientserver.com.example.bjmaclean.testclientserver.util;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bjmaclean.testclientserver.R;

import java.util.List;

public class TournamentAdapter extends RecyclerView.Adapter<TournamentAdapter.MyViewHolder> {

    private List<Tournament> tournamentList;



    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView name, startDate;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            startDate = (TextView) view.findViewById(R.id.startDate);

        }

        @Override
        public void onClick(View v) {
            Toast.makeText(v.getContext(), "clicked", Toast.LENGTH_LONG).show();
        }
    }


    public TournamentAdapter(List<Tournament> tournamentList) {
        this.tournamentList = tournamentList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tournament_list_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Tournament tournament = tournamentList.get(position);
        holder.name.setText(tournament.getTournamentName());
        holder.startDate.setText(tournament.getTournamentStartDate());

        //************************************************************************
        //Storing the tournament id in a final attribute.  It can then be used
        //in the onClickListener to know which tournament is clicked
        //http://stackoverflow.com/questions/39074272/respond-to-buttons-click-in-recyclerview
        //************************************************************************

        final int  tournamentId = tournament.getTournamentId();
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "clicked "+tournamentId, Toast.LENGTH_SHORT).show();
            }
        });
        holder.startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "clicked "+tournamentId, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return tournamentList.size();
    }
}
