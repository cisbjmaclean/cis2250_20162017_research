package info.hccis.fragments20162017;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.Toast;
import android.widget.ToggleButton;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link FragmentCampbellPat#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentCampbellPat extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private RatingBar ratingBar3 = null;
    private ToggleButton toggleButton3 = null;
    private ToggleButton toggleButton = null;
    private String mParam1;
    private String mParam2;

    private OnFragmentCampbellPatInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentPhilip.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentCampbellPat newInstance(String param1, String param2) {
        FragmentCampbellPat fragment = new FragmentCampbellPat();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentCampbellPat() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_campbell_pat, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String message) {
        if (mListener != null) {
            mListener.onFragmentCampbellPatInteraction(message);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentCampbellPatInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public void onStart(){
        super.onStart();

        ratingBar3 = (RatingBar)getActivity().findViewById(R.id.ratingBar3);
        toggleButton3 = (ToggleButton)getActivity().findViewById(R.id.toggleButton3);

        toggleButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleButton toggleButton2 = (ToggleButton)v;
                String option =  toggleButton2.getText().toString();
                if(option.equalsIgnoreCase("On")){
                    ratingBar3.setVisibility(View.VISIBLE);
                }
                else if(option.equalsIgnoreCase("OFF")){
                    ratingBar3.setVisibility(View.INVISIBLE);
                }
                else{
                    ratingBar3.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentCampbellPatInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentCampbellPatInteraction(String message);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        ImageButton button = (ImageButton) getView().findViewById(R.id.imageButton1);
        ratingBar3 = (RatingBar)getActivity().findViewById(R.id.ratingBar3);
        toggleButton3 = (ToggleButton)getActivity().findViewById(R.id.toggleButton3);

        button.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Toast.makeText(getActivity(),
                        String.valueOf(ratingBar3.getRating()),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}
