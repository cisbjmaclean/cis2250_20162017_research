package info.hccis.fragments20162017;

        import android.app.Activity;
import android.os.Bundle;
        import android.support.v4.app.Fragment;
        import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.CalendarView.OnDateChangeListener;
import android.widget.RatingBar;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link FragmentChevrierJason#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentChevrierJason extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private RatingBar ratingBar;
    private Button btnSubmit;
    private CalendarView calendar;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentChevrierJasonInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentPhilip.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentChevrierJason newInstance(String param1, String param2) {
        FragmentChevrierJason fragment = new FragmentChevrierJason();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentChevrierJason() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("Fragment lifecycle", "onCreateView triggered.");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chevrier_jason, container, false);

        ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
        btnSubmit = (Button) view.findViewById(R.id.btnSubmit);

        //initializes the calendarview
        initializeCalendar(view);

        addListenerOnButton();

        return view;
    }


    public void initializeCalendar(View view) {

        calendar = (CalendarView) view.findViewById(R.id.calendarView);

        // sets whether to show the week number.
        calendar.setShowWeekNumber(false);


        // here we set Monday as the first day of the Calendar
        calendar.setFirstDayOfWeek(2);

        //sets the listener to be notified upon selected date change.
        calendar.setOnDateChangeListener(new OnDateChangeListener() {

            //show the selected date as a toast
            @Override
            public void onSelectedDayChange(CalendarView fragView, int year, int month, int day) {
                Log.d("***********", "OnChangeListenerRunning1");
                Toast.makeText(fragView.getContext(), day + "/" + (month + 1) + "/" + year, Toast.LENGTH_SHORT).show();
                Log.d("OnChange Listener", "OnChangeListenerRunning");
            }
        });

    }


    public void addListenerOnButton() {

        //if click on me, then display the current rating value.
        btnSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Toast.makeText(v.getContext(), String.valueOf(ratingBar.getRating()), Toast.LENGTH_SHORT).show();

            }
        });

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String message) {
        if (mListener != null) {
            mListener.onFragmentChevrierJasonInteraction(message, 5);
        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentChevrierJasonInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */


    public interface OnFragmentChevrierJasonInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentChevrierJasonInteraction(String message, int number);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

    }

}
