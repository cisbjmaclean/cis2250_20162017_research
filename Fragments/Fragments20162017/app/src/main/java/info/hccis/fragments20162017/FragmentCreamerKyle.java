package info.hccis.fragments20162017;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentCreamerKyle newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentCreamerKyle extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters

    private RatingBar ratingBar;
    private TextView textView;
    private OnFragmentCKInteractionListener mListener;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BlankFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Fragment FragmentCreamerKyle(String param1, String param2) {
        FragmentCreamerKyle fragment = new FragmentCreamerKyle();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentCreamerKyle() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d("Fragment lc-Fragment", "onCreate triggered.");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        Log.d("Fragment lc-Fragment", "onAttach triggered.");
        super.onAttach(activity);

        try {
            mListener = (OnFragmentCKInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_creamer_kyle, container, false);
//        Switch btnSwitch = (Switch) view.findViewById(R.id.switch1);
//        ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
//        textView = (TextView) view.findViewById(R.id.textView);
//
//        btnSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    Log.i("Switch", "ON");
//                    ratingBar.setNumStars(5);
//                    ratingBar.setRating(5);
//                    CharSequence stars = "You get five stars!";
//                    textView.setText(stars);
//                    mListener.OnFragmentCKInteraction("Fragment says you get five stars!");
//                } else {
//                    Log.i("Switch", "OFF");
//                    ratingBar.setNumStars(0);
//                    ratingBar.setRating(0);
//                    CharSequence stars = "You lost five stars!";
//                    textView.setText(stars);
//                    mListener.OnFragmentCKInteraction("Fragment says you get no stars.");
//
//                }
//            }
//        });
        return view;


    }

    public interface OnFragmentCKInteractionListener {
        // TODO: Update argument type and name
        public void OnFragmentCKInteraction(String name);
    }

}
