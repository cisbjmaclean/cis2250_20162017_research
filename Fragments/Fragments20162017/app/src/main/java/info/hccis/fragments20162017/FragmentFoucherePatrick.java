package info.hccis.fragments20162017;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentFoucherePatrick.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentFoucherePatrick#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentFoucherePatrick extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private Button buttonHello;
    private List<Integer> spinnerArray = new ArrayList<Integer>();
    private Spinner theSpinner;
    private Switch theSwitch;
    private int number=0;
    private Boolean toastCheck;

    public FragmentFoucherePatrick() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentFoucherePatrick.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentFoucherePatrick newInstance(String param1, String param2) {
        FragmentFoucherePatrick fragment = new FragmentFoucherePatrick();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fouchere_patrick, container, false);
        //creates a switch and uses an onchecked change listener to pull if it's checked or not checked.
        theSwitch = (Switch) view.findViewById(R.id.switch1);
        //This is confusing because switch inherits CompoundButton so they are essentially one in the same just different design style.
        theSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //Assigns value to our switch to use later on.
                if(isChecked){
                    toastCheck=true;
                }else{
                    toastCheck=false;
                }
            }
        });
        buttonHello = (Button) view.findViewById(R.id.buttonHello);
        buttonHello.setOnClickListener(new View.OnClickListener() {
                                           public void onClick(View view) {
                                               //pulls the selected index from our spinner of numbers.
                                                 number = (Integer) theSpinner.getSelectedItem();
                                                 String numString = Integer.toString(number);
                                               //determines what will happen once the button is clicked based on the switch status.
                                                 if(toastCheck){
                                                     Toast.makeText(getActivity(), "You selected the number: "+numString,
                                                             Toast.LENGTH_LONG).show();
                                                 }else{
                                                     Log.d("Output:","You selected "+numString+" but didn't want people to find out");
                                                 }


                                           }
                                       }
        );
        //Loads array from 1-100
        for (int i = 1; i <= 100; i++) {
            spinnerArray.add(i);
        }
        //Fills the spinner with the array using adapter
        theSpinner = (Spinner) view.findViewById(R.id.spinner);
        ArrayAdapter<Integer> adp = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_spinner_item, spinnerArray);
        adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        theSpinner.setAdapter(adp);

        return view;
    }


    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragment1Interaction(String theString);
    }
}
