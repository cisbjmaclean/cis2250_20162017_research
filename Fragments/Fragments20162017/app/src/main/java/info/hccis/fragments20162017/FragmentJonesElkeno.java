package info.hccis.fragments20162017;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.QuickContactBadge;
import android.widget.SeekBar;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentElkenoInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentJonesElkeno#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentJonesElkeno extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private int[] numbers = new int[]{1, 2, 3, 4, 6, 7, 8, 9, 10};
    private int curentNumber;
    private TextView textView;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentElkenoInteractionListener mListener;

    public FragmentJonesElkeno() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentJonesElkeno.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentJonesElkeno newInstance(String param1, String param2) {
        FragmentJonesElkeno fragment = new FragmentJonesElkeno();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_jones_elkeno, container, false);
        Button helloButton = (Button) view.findViewById(R.id.buttonHello);
        helloButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Log.d("Fragment elkenoFrag", "The button created by Elkeno was clicked");
                Log.d("Fragment elkenoFrag", "now triggering the code in the activity!!");
                mListener.onFragmentElkenoInteraction("This is a message from the Elkeno's fragment!");

            }
        });

        QuickContactBadge badge = (QuickContactBadge) view.findViewById(R.id.quickContactBadge);
        badge.assignContactFromPhone("9021234567", true);


        curentNumber = 0;
        SeekBar seekBar = (SeekBar) view.findViewById(R.id.seekBar);
        seekBar.setMax(numbers.length + 1);
        System.out.println("Seekbar loaded. Max number is: " + (numbers.length + 1));

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                curentNumber = progressValue;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                textView.setText("Progess is: " + curentNumber + "/" + (numbers.length + 1));
            }
        });
        System.out.println("Made it passed seekbar change listener");
        textView = (TextView) view.findViewById(R.id.textInArray);
        textView.setText("You are at: " + numbers[0] + "/" + (numbers.length + 1));
        System.out.println("here");

        return view;
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentElkenoInteraction("onButtonPressed");
        }
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnFragmentElkenoInteractionListener) {
            mListener = (OnFragmentElkenoInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentElkenoInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentElkenoInteractionListener {
        // TODO: Update argument type and name
        void onFragmentElkenoInteraction(String message);
    }
}
