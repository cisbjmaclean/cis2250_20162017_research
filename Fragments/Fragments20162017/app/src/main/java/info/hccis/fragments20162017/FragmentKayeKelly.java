package info.hccis.fragments20162017;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TimePicker;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentKayeKelly.OnFragmentKayeKellyInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentKayeKelly#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentKayeKelly extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


       // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private OnFragmentKayeKellyInteractionListener mListener;

    public FragmentKayeKelly() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentKayeKelly.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentKayeKelly newInstance(String param1, String param2) {
        FragmentKayeKelly fragment = new FragmentKayeKelly();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_kaye_kelly, container, false);
        Button clickMe = (Button) view.findViewById(R.id.button);

        final TimePicker timePicker = (TimePicker) view.findViewById(R.id.timePicker);

        clickMe.setOnClickListener(new View.OnClickListener() {
                                           public void onClick(View view) {
                                               if(timePicker.getVisibility()==TimePicker.GONE) {
                                                   timePicker.setVisibility(TimePicker.VISIBLE);
                                               }
                                               else {
                                                   timePicker.setVisibility(TimePicker.GONE);
                                               }
                                               Log.d("BJTEST", "The button was clicked");
                                               mListener.onFragmentKayeKellyInteraction("Message from fragment :-)");
                                           }
                                       }
        );
        Log.d("BJTEST", "onCreate run in Activity 1");
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentKayeKellyInteraction("message");
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try  {
            mListener = (OnFragmentKayeKellyInteractionListener) activity;
        } catch (RuntimeException rte) {
            throw new RuntimeException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentKayeKellyInteractionListener {
        // TODO: Update argument type and name
        void onFragmentKayeKellyInteraction(String message);
    }
}
