package info.hccis.fragments20162017;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentMacGregorMajor.OnFragmentMacGregorMajorInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentMacGregorMajor#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentMacGregorMajor extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters

    private ToggleButton toggle;
    private RatingBar rate;
    private TextView textView;
    private View view;

    private OnFragmentMacGregorMajorInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Fragment1.
     */

    public static FragmentMacGregorMajor newInstance(String param1, String param2) {
        Log.d("Fragment lc-Fragment", "newInstance being triggered.");
        FragmentMacGregorMajor fragment = new FragmentMacGregorMajor();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentMacGregorMajor() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d("Fragment lc-Fragment", "onCreate triggered.");
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        view = inflater.inflate(R.layout.fragment_major_macgregor, container, false);

        rate = (RatingBar)getActivity().findViewById(R.id.ratingBar);
        toggle = (ToggleButton) view.findViewById(R.id.toggleButton);
        textView = (TextView) view.findViewById(R.id.textView);





        toggle.setOnCheckedChangeListener( new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton toggleButton, boolean isChecked) {

                if (isChecked) {
                    textView.setText("Turn lights on");
                    textView.setTextColor(-1);
                    view.setBackgroundColor(0xff000000);
                    Toast toast = Toast.makeText(getActivity(), "Lights are off!", Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    textView.setText("Turn lights off");
                    textView.setTextColor(0xff000000);
                    view.setBackgroundColor(-1);
                    Toast toast = Toast.makeText(getActivity(), "Lights are on!", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        }) ;



        return view;
    }


    public void onStart(){
        super.onStart();

        rate = (RatingBar)getActivity().findViewById(R.id.ratingBar);
        rate.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                if (rating>=3){
                    Toast toast = Toast.makeText(getActivity(), "Your rating is: "+String.valueOf(rating)+", :)", Toast.LENGTH_SHORT);
                    toast.show();
                }else if (rating<=2.5){
                    Toast toast = Toast.makeText(getActivity(), "Your rating is: "+String.valueOf(rating)+", :(", Toast.LENGTH_SHORT);
                    toast.show();
                }


                Log.d("Rating",String.valueOf(rating));
            }
        });

    }

    @Override
    public void onAttach(Activity activity) {
        Log.d("Fragment lc-Fragment", "onAttach triggered.");
        super.onAttach(activity);

        try {
            mListener = (OnFragmentMacGregorMajorInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }


    }

    @Override
    public void onDetach() {
        Log.d("Fragment lc-Fragment", "onDetach triggered.");
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentMacGregorMajorInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentMacGregorMajorInteraction(String name);
    }

}
