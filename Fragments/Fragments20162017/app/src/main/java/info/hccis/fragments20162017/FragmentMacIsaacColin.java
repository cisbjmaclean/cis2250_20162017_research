package info.hccis.fragments20162017;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.CheckBox;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link FragmentMacIsaacColin#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentMacIsaacColin extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;


    //Attributes required for the buttons and switches
    private Button myButton;
    private ToggleButton myToggle;
    private TextView toggleState;
    private CheckBox myCheck;
    private TextView checkState;
    private int clickTotal = 0;
    private TextView clickText;

    private OnFragmentMacIsaacColinInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentPhilip.
     */
    public static FragmentMacIsaacColin newInstance(String param1, String param2) {
        FragmentMacIsaacColin fragment = new FragmentMacIsaacColin();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentMacIsaacColin() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //inflate the fragment view
        View view = inflater.inflate(R.layout.fragment_macisaac_colin, container, false);

        //creating the text and
        clickText = (TextView) view.findViewById(R.id.clickNum);
        //match the switch and status string to the elements on the layout
        checkState = (TextView) view.findViewById(R.id.checkStatus);
        myCheck = (CheckBox) view.findViewById(R.id.bigCheck);
        //attach the event listener to the switch
        myCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //if the switch has been enabled
                    checkState.setText("Checkbox is ON");
                } else {
                    // The switch is disabled
                    checkState.setText("Checkbox is OFF");
                }
                updateNum();
            }
        });

        //match the toggle and status string to the elements on the layout
        toggleState = (TextView) view.findViewById(R.id.toggleStatus);
        myToggle = (ToggleButton) view.findViewById(R.id.bigToggle);
//        attach the event listener to the toggle
        myToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //if the toggle has been enabled
                    toggleState.setText("Toggle is ON");
                } else {
                    // The toggle is disabled
                    toggleState.setText("Toggle is OFF");
                }
                updateNum();;
            }
        });

        return view;
    }

    public void onButtonPressed(String message) {
        if (mListener != null) {
            mListener.onFragmentMacIsaacColinInteraction(message);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentMacIsaacColinInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentMacIsaacColinInteractionListener {
        void onFragmentMacIsaacColinInteraction(String message);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        myButton = (Button) getView().findViewById(R.id.bigButton);
        myToggle = (ToggleButton) getView().findViewById(R.id.bigToggle);
        myCheck = (CheckBox) getView().findViewById(R.id.bigCheck);

        myButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                Toast.makeText(getActivity(),"you clicked the button!", Toast.LENGTH_SHORT).show();
                updateNum();
            }

        });


    }

    public void updateNum (){
        clickTotal++;
        clickText.setText(""+clickTotal);
    }

}