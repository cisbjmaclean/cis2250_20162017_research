package info.hccis.fragments20162017;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentMaderNeil.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentMaderNeil#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentMaderNeil extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    ImageView img;
    TextView finalResult;
    RadioGroup rFruit=null;

    RadioButton apples;
    RadioButton oranges;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentMaderNeil() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentMaderNeil.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentMaderNeil newInstance(String param1, String param2) {
        FragmentMaderNeil fragment = new FragmentMaderNeil();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }


//    public void fruitSelection(View view) {
//
//
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_fragment_mader_neil, container, false);

        apples = (RadioButton) view.findViewById(R.id.apples);
        oranges = (RadioButton) view.findViewById(R.id.oranges);

        finalResult = (TextView)view.findViewById(R.id.fruitResult);
//        finalResult.setEnabled(false);
        /* Inflate the layout for this fragment */

        rFruit = (RadioGroup)view.findViewById(R.id.radioFruit);

        img = (ImageView) view.findViewById(R.id.fruitPicture);
//        boolean checked = ((RadioButton) view).isChecked();

        rFruit.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {

                    case R.id.apples:
//               if (checked) {
                        finalResult.setText("Great idea!");
                        finalResult.setEnabled(true);
                        img.setImageResource(R.drawable.apple);
                        System.out.println("We are there!!!!!!");
//                } else {
//
//                    finalResult.setEnabled(false);
//                }
                        break;

                    case R.id.oranges:
//                if (checked) {
                        finalResult.setText("Bad idea!");
                        finalResult.setEnabled(true);
                        img.setImageResource(R.drawable.orange);
//                } else {
//                    finalResult.setEnabled(false);
//                }
                        break;
                }
            }
        });

        return view;
    }
    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

//    @Override
//    public void onActivityCreated(Bundle savedInstanceState){
//        super.onActivityCreated(savedInstanceState);
//
//    }
}
