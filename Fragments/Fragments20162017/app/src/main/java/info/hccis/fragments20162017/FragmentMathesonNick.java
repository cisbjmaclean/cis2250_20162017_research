package info.hccis.fragments20162017;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TimePicker;
import android.widget.RadioGroup.OnCheckedChangeListener;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentMathesonNick.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentMathesonNick#newInstance} factory method to
 * create an instance of this fragment.
 *
 * @author Nick Matheson
 * @since 01182016
 */
public class FragmentMathesonNick extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private RadioButton twelveHour = null;
    private RadioButton twentyFourHour = null;

    private TimePicker tp = null;
    private Button buttonGo = null;
    private RadioGroup rg = null;


    private OnFragmentInteractionListener mListener;

    public FragmentMathesonNick() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentMathesonNick.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentMathesonNick newInstance(String param1, String param2) {
        FragmentMathesonNick fragment = new FragmentMathesonNick();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_matheson_nick, container, false);
        twelveHour = (RadioButton) view.findViewById(R.id.radioButton12Hour);
        twentyFourHour = (RadioButton) view.findViewById(R.id.radioButton24Hour);
        tp = (TimePicker) view.findViewById(R.id.timePicker);
        rg = (RadioGroup) view.findViewById(R.id.radioGroup);

        rg.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            // Is the button now checked?
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected


                switch (checkedId) {
                    //if the 12 hour radio button is checked, set the time picker to 12 hour mode
                    case R.id.radioButton12Hour:
                        tp.setIs24HourView(false);
                        break;
                    //if the 24 hour radio button is checked, set the time picker to 24 hour mode
                    case R.id.radioButton24Hour:
                        tp.setIs24HourView(true);
                        break;
                }

            }
        });

        buttonGo = (Button) view.findViewById((R.id.buttonGo));
        buttonGo.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                //creates variables from the time picker
                int hour = tp.getCurrentHour();
                int mins = tp.getCurrentMinute();

                String fullHour;
                String fullMins;

                //sets the output to be 12 hour view
                if (!tp.is24HourView()){
                    if (hour == 0){
                        hour = 12;
                    } else if (hour > 12){
                        hour = hour - 12;
                    }
                }

                //sets the hours to be double digits
                if (hour < 10){
                    fullHour = "0"+hour;
                } else {
                    fullHour = ""+hour;
                }

                //sets the minutes to be double digits
                if (mins < 10){
                    fullMins = "0"+mins;
                } else {
                    fullMins = ""+mins;
                }

                //displays the time to console
                Log.d("TIME", fullHour+":"+fullMins);

            }
        });

        return view;
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
