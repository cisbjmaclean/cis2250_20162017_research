package info.hccis.fragments20162017;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link FragmentPeconiJamison#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentPeconiJamison extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    // Initialize Items from fragment
    private int progressBarStatus = 0;
    private Handler handler = new Handler();


    private OnFragmentPeconiJamisonInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentPhilip.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentPeconiJamison newInstance(String param1, String param2) {
        FragmentPeconiJamison fragment = new FragmentPeconiJamison();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentPeconiJamison() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Grab a reference to the view
        final View v = inflater.inflate(R.layout.fragment_peconi_jamison, container, false);


        // Grab the widgets from the screen
        final Button btn = (Button) v.findViewById(R.id.downloadButton);
        final ProgressBar progressBar = (ProgressBar) v.findViewById(R.id.progressBar2);
        final TextView progressLabel = (TextView) v.findViewById(R.id.txtProgress);
        final ProgressBar smallSpinner = (ProgressBar) v.findViewById(R.id.spinnerSmall);
        final ProgressBar largeSpinner = (ProgressBar) v.findViewById(R.id.spinnerLarge);
        progressLabel.setVisibility(View.INVISIBLE);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onFragmentPeconiJamisonInteraction("FoucherePatrick");





                progressLabel.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                smallSpinner.setVisibility(View.VISIBLE);
                largeSpinner.setVisibility(View.VISIBLE);

                // Reset the progress everytime the button is clicked
                // This will restart the simulated download
                progressBarStatus = 0;

                // Start a thread which will work as our simulated download
                // Its a simple loop that pauses for 50 milliseconds before adding 1
                // Stops when the counter gets to 100
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while(progressBarStatus < 100){
                            // Update the progress status
                            progressBarStatus +=1;

                            try{
                                Thread.sleep(100);
                            }catch(InterruptedException e){
                                e.printStackTrace();
                            }

                            // Second thread which runs concurrently and
                            // updates the progress bar by passing in the current
                            // value of progress bar status variable from the other thread
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    progressBar.setProgress(progressBarStatus);
                                    // Update the label of the download progress
                                    progressLabel.setText(progressBarStatus + " %");
                                    // Display toast to the screen that download is complete
                                    if(progressBarStatus == 100) {
                                        Toast.makeText(v.getContext(), "Download Complete",
                                                Toast.LENGTH_SHORT).show();
                                        progressLabel.setVisibility(View.INVISIBLE);
                                        progressBar.setVisibility(View.INVISIBLE);
                                        smallSpinner.setVisibility(View.INVISIBLE);
                                        largeSpinner.setVisibility(View.INVISIBLE);
                                    }
                                }
                            });
                        }
                    }
                }).start(); // Start the progress bar when button is clicked
            }
        });
        return v;
    }




    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String message) {
        if (mListener != null) {
            mListener.onFragmentPeconiJamisonInteraction(message);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentPeconiJamisonInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentPeconiJamisonInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentPeconiJamisonInteraction(String message);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){

        Log.d("debugging","onActivityCreated triggered in Jamison frament");
        super.onActivityCreated(savedInstanceState);

    }

}
