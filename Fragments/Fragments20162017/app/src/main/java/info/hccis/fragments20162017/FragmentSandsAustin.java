package info.hccis.fragments20162017;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link FragmentSandsAustin#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentSandsAustin extends Fragment
{
	// TODO: Rename parameter arguments, choose names that match
	// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
	private static final String ARG_PARAM1 = "param1";
	private static final String ARG_PARAM2 = "param2";

	// TODO: Rename and change types of parameters
	private String mParam1;
	private String mParam2;
	private Button nextBtn = null;
	private Button backBtn;
	private RatingBar ratingBar3 = null;
	private ImageView imageView2;

	private OnFragmentSandsAustinInteractionListener mListener;

	/**
	 * Use this factory method to create a new instance of
	 * this fragment using the provided parameters.
	 *
	 * @param param1 Parameter 1.
	 * @param param2 Parameter 2.
	 * @return A new instance of fragment FragmentPhilip.
	 */
	// TODO: Rename and change types and number of parameters
	public static FragmentSandsAustin newInstance(String param1, String param2)
	{
		FragmentSandsAustin fragment = new FragmentSandsAustin();
		Bundle args = new Bundle();
		args.putString(ARG_PARAM1, param1);
		args.putString(ARG_PARAM2, param2);
		fragment.setArguments(args);
		return fragment;
	}

	public FragmentSandsAustin()
	{
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if (getArguments() != null)
		{
			mParam1 = getArguments().getString(ARG_PARAM1);
			mParam2 = getArguments().getString(ARG_PARAM2);
		}

/*		nextBtn.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View v)
			{
				String say = ratingBar3.toString();
				Toast.makeText(getActivity(), say, Toast.LENGTH_SHORT).show();
			}
		});*/

	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_sands_austin, container, false);
		nextBtn = (Button) view.findViewById(R.id.nextBtn);
		backBtn = (Button) view.findViewById(R.id.backBtn);
		ratingBar3 = (RatingBar) view.findViewById(R.id.ratingBar3);
		imageView2 = (ImageView) view.findViewById(R.id.imageView2);

		nextBtn.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				float uN = ratingBar3.getRating();
				String message = "You gave the picture a rating of " + uN + " stars.";
				Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
				imageView2.setImageResource(R.drawable.tree);
			}
		});

		backBtn.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				imageView2.setImageResource(R.drawable.dirt);
			}
		});


		// Inflate the layout for this fragment
		return view;
	}

	/*@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState)
	{
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_sands_austin, container, false);
	}*/

	// TODO: Rename method, update argument and hook method into UI event
	public void onButtonPressed(String message)
	{
		if (mListener != null)
		{
			mListener.onFragmentSandsAustinInteraction(message);

		}
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		try
		{
			mListener = (OnFragmentSandsAustinInteractionListener) activity;
		} catch (ClassCastException e)
		{
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach()
	{
		super.onDetach();
		mListener = null;
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated
	 * to the activity and potentially other fragments contained in that
	 * activity.
	 * <p/>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFragmentSandsAustinInteractionListener
	{
		// TODO: Update argument type and name
		public void onFragmentSandsAustinInteraction(String message);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
	}
}
