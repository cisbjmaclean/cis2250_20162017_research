package info.hccis.fragments20162017;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;


/**
 * @author Silvia Gigova
 * @since :2017-01-22
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link Fragment_GigovaSilvia} factory method to
 * create an instance of this fragment.
 */
public class Fragment_GigovaSilvia extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Button increase=null;
    private  Button decrease=null;
    private ProgressBar prg=null;
    private int int_ventilator;

    private OnFragmentGigovaSilviaInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentPhilip.
     */
    // TODO: Rename and change types and number of parameters
    public static Fragment_GigovaSilvia newInstance(String param1, String param2) {
        Fragment_GigovaSilvia fragment = new Fragment_GigovaSilvia();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public Fragment_GigovaSilvia() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_gigova_silvia, container, false);
        prg = (ProgressBar)view.findViewById(R.id.ventilator_progress);
        decrease = (Button) view.findViewById(R.id.decrease);
        increase = (Button) view.findViewById(R.id.increase);
        decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(int_ventilator > 0){
                    int_ventilator = int_ventilator - 10;
                }
                prg.setProgress(int_ventilator);
                Log.d(TAG,"Ventilator progresas " + int_ventilator);
            }
        });
        increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(int_ventilator < 100) {
                    int_ventilator = int_ventilator + 10;
                }
                prg.setProgress(int_ventilator);
                Log.d(TAG,"Ventilator progresas " + int_ventilator);
            }
        });

        Spinner spinner;

        spinner = (Spinner)view.findViewById(R.id.spinner);

        // Spinner click listener

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Automobile");
        categories.add("Business Services");
        categories.add("Computers");
        categories.add("Education");
        categories.add("Personal");
        categories.add("Travel");

        // Creating adapter for spinner

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos,
                                       long id) {

                Toast.makeText(parent.getContext(),
                        "On Item Select : \n" + parent.getItemAtPosition(pos).toString(),
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        final RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                RadioButton rb = null;
                switch(checkedId) {
                    case R.id.radioButton1:
                      rb=(RadioButton)view.findViewById(R.id.radioButton1);
                       break;
                    case R.id.radioButton2:
                        rb=(RadioButton)view.findViewById(R.id.radioButton2);
                        break;
                    case R.id.radioButton3:
                        rb=(RadioButton)view.findViewById(R.id.radioButton3);
                        break;
                }


                Toast.makeText(view.getContext(),"You choose "+ rb.getText().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });

        return view;
        //return inflater.inflate(R.layout.fragment_gigova_silvia, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String message) {
        if (mListener != null) {
            mListener.onFragmentGigovaSilviaInteraction(message);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentGigovaSilviaInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentGigovaSilviaInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentGigovaSilviaInteraction(String message);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

    }

}
