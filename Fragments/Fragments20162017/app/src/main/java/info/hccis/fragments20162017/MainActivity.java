package info.hccis.fragments20162017;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


public class MainActivity extends ActionBarActivity implements FragmentSandsAustin.OnFragmentSandsAustinInteractionListener,
        FragmentMaderNeil.OnFragmentInteractionListener,
        FragmentPeconiJamison.OnFragmentPeconiJamisonInteractionListener,
        FragmentCreamerKyle.OnFragmentCKInteractionListener,
        FragmentChevrierJason.OnFragmentChevrierJasonInteractionListener,
        FragmentFoucherePatrick.OnFragmentInteractionListener,
        FragmentJonesElkeno.OnFragmentElkenoInteractionListener,
        FragmentTing.OnFragmentTingInteractionListener,
        FragmentCampbellPat.OnFragmentCampbellPatInteractionListener,
        Fragment_GigovaSilvia.OnFragmentGigovaSilviaInteractionListener,
        FragmentMathesonNick.OnFragmentInteractionListener,
        FragmentKayeKelly.OnFragmentKayeKellyInteractionListener,
        FragmentMacGregorMajor.OnFragmentMacGregorMajorInteractionListener,
        FragmentMacIsaacColin.OnFragmentMacIsaacColinInteractionListener,
        FragmentBuchananBlake.OnFragmentBuchananBlakeInteractionListener {


    FragmentManager fm = this.getSupportFragmentManager();
    FragmentTransaction ft = fm.beginTransaction();
    FragmentNotImplemented fragment1;
    private String chosen = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            chosen = savedInstanceState.getString("chosenFragment");
        }

        setContentView(R.layout.activity_main);
        ft.replace(R.id.fragment, getAppropriateFragment(chosen));
        ft.commit();

        final ListView lv = (ListView) findViewById(R.id.listViewNames);
        lv.setClickable(true);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                Object o = lv.getItemAtPosition(position);
//                Toast.makeText(getApplicationContext(),
//                        o.toString(), Toast.LENGTH_SHORT).show();

                chosen = (String) o;
                Log.d("BJTEST", "chosen item from listview = " + chosen);

                Fragment newFragment = getAppropriateFragment(chosen);
                ft = fm.beginTransaction();
                ft.replace(R.id.fragment, newFragment);
                ft.commit();
            }
        });


    }

    @Override
    public void onSaveInstanceState(Bundle saveInstanceState) {
        Log.d("debugging", "onSaveInstanceState triggered");
        saveInstanceState.putString("chosenFragment", chosen);
        super.onSaveInstanceState(saveInstanceState);
    }

    @Override
    protected void onStop() {
        Log.d("debugging", "onStop triggered");
        super.onStop();
    }

    /**
     * This method will return the fragment associated with the name passed in.  It will return
     * an instance of FragmentNotImplemented if no match for the given name.
     *
     * @param fragmentName
     * @return the appropriate fragment
     * @author BJM CIS2250
     * @since 20170131
     */

    public Fragment getAppropriateFragment(String fragmentName) {
        Fragment newFragment = null;
        switch (fragmentName) {
            case "ImageView":
                newFragment = new FragmentImageViewer();
                break;
            case "BuchananBlake":
                newFragment = new FragmentBuchananBlake();
                break;
            case "CampbellPatrick":
                newFragment = new FragmentCampbellPat();
                break;
            case "MaderNeil":
                newFragment = new FragmentMaderNeil();
                break;
            case "PeconiJamison":
                newFragment = new FragmentPeconiJamison();
                break;
            case "CreamerKyle":
                newFragment = new FragmentCreamerKyle();
                break;
            case "ChevrierJason":
                newFragment = new FragmentChevrierJason();
                break;
            case "FoucherePatrick":
                newFragment = new FragmentFoucherePatrick();
                break;
            case "GigovaSilvia":
                newFragment = new Fragment_GigovaSilvia();
                break;
            case "KayeKelly":
                newFragment = new FragmentKayeKelly();
                break;
            case "MacGregorMajor":
                newFragment = new FragmentMacGregorMajor();
                break;
            case "MacIsaacColin":
                newFragment = new FragmentMacIsaacColin();
                break;
            case "MathesonNick":
                newFragment = new FragmentMathesonNick();
                break;
            case "JonesElkeno":
                newFragment = new FragmentJonesElkeno();
                break;
            case "SandsAustin":
                newFragment = new FragmentSandsAustin();
                break;
            case "Ting":
                newFragment = new FragmentTing();
                break;
            default:
                newFragment = new FragmentNotImplemented();
        }
        return newFragment;

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentPeconiJamisonInteraction(String message) {
        Log.d("Fragment interaction", message);
        Fragment newFragment = getAppropriateFragment(message);
        ft = fm.beginTransaction();
        ft.replace(R.id.fragment, newFragment);
        ft.commit();
    }

    @Override
    public void onFragmentKayeKellyInteraction(String message) {

    }

    @Override
    public void OnFragmentCKInteraction(String name) {
        Log.d("Fragment interaction", name);
    }

    @Override
    public void onFragmentChevrierJasonInteraction(String message, int number) {
        Log.d("Fragment interaction", message + number);
    }

    @Override
    public void onFragment1Interaction(String theString) {
        Log.d("Fragment interaction", theString);
    }

    @Override
    public void onFragmentElkenoInteraction(String message) {
        Log.d("Fragment interaction", message);
    }

    @Override
    public void OnFragmentTingInteractionListener(String message) {
        Log.d("Fragment interaction", message);
    }

    @Override
    public void onFragmentCampbellPatInteraction(String message) {
        Log.d("Fragment interaction", message);
    }

    @Override
    public void onFragmentGigovaSilviaInteraction(String message) {
        Log.d("Fragment interaction", message);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }

    @Override
    public void onFragmentMacGregorMajorInteraction(String name) {

    }

    @Override
    public void onFragmentMacIsaacColinInteraction(String message) {

    }

    @Override
    public void onFragmentBuchananBlakeInteraction(String message) {

    }

    @Override
    public void onFragmentSandsAustinInteraction(String message) {

    }
}
