package com.example.jchevrier112091.facebookapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    Button b1;
    Button b2;
    TextView tv;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1 = (Button)findViewById(R.id.button);
        b2 = (Button)findViewById(R.id.button2);

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                tv = (TextView)findViewById(R.id.textView2);

                // creates a new intent when the shareButton is clicked.
                Intent textIntent = new Intent();
                //Specifies that the purpose of this intent is to send data
                textIntent.setAction(Intent.ACTION_SEND);
                //gets the text from the TextView box
                textIntent.putExtra(Intent.EXTRA_TEXT, "");
                //Specifies type of data to be sent
                textIntent.setType("text/plain");
                //Opens the chooser with and sends the data within the intent to the selected application.
                startActivity(Intent.createChooser(textIntent, "Share text using..."));
            }
        });

        b1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);

                Uri screenshotUri = Uri.parse("android.resource://com.example.jchevrier112091.facebookapp/mipmap/" + R.mipmap.ic_launcher);

                sharingIntent.setType("image/png");

                sharingIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);

                startActivity(Intent.createChooser(sharingIntent, "Share image using..."));
            }
        });
    }



//    private void initShareIntent(String type,String _text){
//        File filePath = getFileStreamPath("shareimage.jpg");  //optional //internal storage
//        Intent shareIntent = new Intent();
//        shareIntent.setAction(Intent.ACTION_SEND);
//        shareIntent.putExtra(Intent.EXTRA_TEXT, _text);
//        shareIntent.putExtra(Intent.EXTRA_STREAM,Uri.fromFile(new File(filePath));  //optional//use this when you want to send an image
//        shareIntent.setType("image/jpeg");
//        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        startActivity(Intent.createChooser(shareIntent, "send"));
//    }
}
