package info.hccis.twitterii;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;



public class MainActivity extends AppCompatActivity
{
	private Button tweetBtn;
	private EditText tweetThis;
	private EditText userName;
	private Button viewUsrBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		tweetBtn = (Button) findViewById(R.id.tweetBtn);
		viewUsrBtn = (Button) findViewById(R.id.viewUserBtn);

		tweetBtn.setOnClickListener(new View.OnClickListener()
									{
										public void onClick(View v)
										{
											tweetThis = (EditText) findViewById(R.id.tweetText);
											String tweet = tweetThis.getText().toString();

											tweetThis(tweet);
										}
									}
		);

		viewUsrBtn.setOnClickListener(new View.OnClickListener()
									  {
										  @Override
										  public void onClick(View v)
										  {
											  userName = (EditText) findViewById(R.id.userName);
											  String username = userName.getText().toString();

											  findUser(username);
										  }
									  }
		);
	}

	protected void tweetThis(String text)
	{
		Intent tweetIntent = new Intent(Intent.ACTION_SEND);
		tweetIntent.putExtra(Intent.EXTRA_TEXT, text);
		tweetIntent.setType("text/plain");

		PackageManager packManager = getPackageManager();
		List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);

		boolean twitterAppIsInstalled = false;
		for (ResolveInfo resolveInfo : resolvedInfoList)
		{
			if (resolveInfo.activityInfo.packageName.startsWith("com.twitter.android"))
			{
				tweetIntent.setClassName(
						resolveInfo.activityInfo.packageName,
						resolveInfo.activityInfo.name);
				twitterAppIsInstalled = true;
				break;
			}
		}
		if (twitterAppIsInstalled)
		{
			startActivity(tweetIntent);
		}
		else
		{
			Intent i = new Intent();
			i.putExtra(Intent.EXTRA_TEXT, text);
			i.setAction(Intent.ACTION_VIEW);
			i.setData(Uri.parse("https://twitter.com/intent/tweet?text=" + urlEncode(text)));
			startActivity(i);

			Toast.makeText(this, "Twitter app isn't found", Toast.LENGTH_LONG).show();
		}
	}

	private String urlEncode(String s)
	{
		try
		{
			return URLEncoder.encode(s, "UTF-8");
		} catch (UnsupportedEncodingException e)
		{
			Log.d("ERROR", "UTF-8 should always be supported", e);
			return "";
		}
	}

	protected void findUser(String username)
	{
		try
		{
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/" + username)));
		}
		catch (Exception e)
		{
			Log.d("ERROR", e.getMessage());
		}
	}
}