package com.example.nick.sqlite_example;

/**
 * Created by Nick on 1/18/2017.
 */

public class Journal {

    private long id;
    private String content;
    private String title;
    private String date;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Title:" + title + "\n" +
                "On "+ date + "\n" +
                content;
    }
}
