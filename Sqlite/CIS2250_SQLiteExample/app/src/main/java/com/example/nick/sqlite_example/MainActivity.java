package com.example.nick.sqlite_example;

import java.util.List;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;


public class MainActivity extends ListActivity {
    private JournalDataSource datasource;
    private EditText content = null;
    private EditText title = null;
    private EditText date = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        content = (EditText) findViewById(R.id.editTextContent);
        title = (EditText) findViewById(R.id.editTextTitle);
        date = (EditText) findViewById(R.id.editTextDate);


        datasource = new JournalDataSource(this);
        datasource.open();

        List<Journal> values = datasource.getAllJournals();

        // use the SimpleCursorAdapter to show the
        // elements in a ListView
        ArrayAdapter<Journal> adapter = new ArrayAdapter<Journal>(this,
                android.R.layout.simple_list_item_1, values);
        setListAdapter(adapter);
    }

    // Will be called via the onClick attribute
    // of the buttons in main.xml
    public void onClick(View view) {
        @SuppressWarnings("unchecked")
        ArrayAdapter<Journal> adapter = (ArrayAdapter<Journal>) getListAdapter();
        Journal journal = null;
        switch (view.getId()) {
//            case R.id.add:
//                String[] comments = new String[] { "Cool", "Very nice", "Hate it" };
//                int nextInt = new Random().nextInt(3);
//                // save the new comment to the database
//                comment = datasource.createComment(comments[nextInt]);
//                adapter.add(comment);
//                break;
//            case R.id.delete:
//                if (getListAdapter().getCount() > 0) {
//                    comment = (Comment) getListAdapter().getItem(0);
//                    datasource.deleteComment(comament);
//                    adapter.remove(comment);
//                }
//                break;
            case R.id.buttonSubmit:
                String journalContent = content.getText().toString();
                String journalDate = date.getText().toString();
                String journalTitle = title.getText().toString();
                journal = datasource.createJournal(journalDate, journalTitle, journalContent);
                adapter.add(journal);
                break;
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        datasource.open();
        super.onResume();
    }

    @Override
    protected void onPause() {
        datasource.close();
        super.onPause();
    }

}
