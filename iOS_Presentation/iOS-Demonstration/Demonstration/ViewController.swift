//
//  ViewController.swift
//  Demonstration
//
//  Created by Patrick Campbell on 2017-01-24.
//  Copyright © 2017 Patrick Campbell. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    

    @IBOutlet weak var boxes: UITextField!
    
    @IBOutlet weak var text: UILabel!

    var totalBoxes = 0
    
    @IBAction func submit(_ sender: Any) {

        if let numberBoxes = Int(boxes.text!){
            totalBoxes += Int(boxes.text!)!
            text.text = "You have entered \(numberBoxes) boxes \nTotal boxes: \(totalBoxes)"
        } else {
            text.text = "That's not a number"
        }
    }
    
    @IBAction func remove(_ sender: Any) {
        
        if totalBoxes > 0 {
        if let numberBoxes = Int(boxes.text!){
            totalBoxes -= Int(boxes.text!)!
            text.text = "You have removed \(numberBoxes) boxes \nTotal boxes: \(totalBoxes)"
        } else {
            text.text = "That's not a number"
        }
        } else {
            text.text = "Number cannot be negative"
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        text.text = "";
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

