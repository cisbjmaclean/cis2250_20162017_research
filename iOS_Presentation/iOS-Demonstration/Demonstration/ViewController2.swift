//
//  ViewController2.swift
//  Demonstration
//
//  Created by Patrick Campbell on 2017-01-24.
//  Copyright © 2017 Patrick Campbell. All rights reserved.
//

import Foundation
import UIKit

class ViewController2: UIViewController {

    @IBOutlet weak var totalBoxes: UILabel!
    var totalBoxesText = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        totalBoxes.text = totalBoxesText

        // Do any additional setup after loading the view, typically from a nib.
    }
}
